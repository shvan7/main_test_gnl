# Main test Get Next Line

### Installation

  - clone repos in your gnl folder
  - go into main_test_gnl (folder)
  - run test.sh

-- Check norme :

```sh
$ sh test.sh norme
```

-- Compile all :

```sh
$ sh test.sh cmp
```

-- Run GNL with /dev/random :

```sh
$ sh test.sh random
```

-- Run GNL with multi-file :

```sh
$ sh test.sh multi
```

-- Run GNL with one file and check if leaks :

```sh
$ sh test.sh leaks
```

-- Run GNL with multi-file and check if leaks :

```sh
$ sh test.sh leaks_m
```

-- Clean all :

```sh
$ sh test.sh clean
```

-- Run with another buff_size (second params) :

```sh
$ sh test.sh leaks 10000
```
