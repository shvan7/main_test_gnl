/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/05 13:26:38 by aulima-f          #+#    #+#             */
/*   Updated: 2018/12/05 13:26:49 by aulima-f         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"
#include <stdio.h>
#include <fcntl.h>
#define YEL "\033[1;33m"
#define GRN "\033[0;32m"
#define BLU "\033[1;34m"
#define RED "\033[0;31m"
#define WHT "\033[1;37m"

int main(int ac, char **av)
{
    int i;
    int line = 0;
    int line2 = 0;
    char **data = (char **)malloc(3 * sizeof(char *));
    data[0] = NULL;

    printf("%sBLU%s : \tfile 1\n", BLU, WHT);
    printf("%sYEL%s : \tfile 2\n", YEL, WHT);
    printf("%sRED%s : \tfile 3\n", RED, WHT);
    ft_putchar('\n');
    if (ac > 1)
    {
        int fd0 = open(av[1], O_RDONLY);
        int fd1 = open(av[2], O_RDONLY);
        while ((i = get_next_line(fd0, data)) > 0)
        {
            line++;
            printf("%s(line = %d)(ret = %d) ", YEL, line, i);
            printf("%s[%s]%s \n", BLU, *data, WHT);
            ft_strdel(data);
            if (ac > 2)
            {
                while ((i = get_next_line(fd1, data)) > 0)
                {
                    line2++;
                    printf("%s(line = %d)(ret = %d) ", YEL, line2, i);
                    printf("%s[%s]%s \n", YEL, *data, WHT);
                    ft_strdel(data);
                }
            }
            ft_strdel(data);
        }
    }
    if (ac == 4)
    {
        line = 0;
        int fd2 = open(av[3], O_RDONLY);
        while ((i = get_next_line(fd2, data)) > 0)
        {
            line++;
            printf("%s(line = %d)(ret = %d) ", YEL, line, i);
            if (fd2 > 0)
                printf("%s[%s]%s \n", RED, *data, WHT);
            ft_strdel(data);
        }
    }
    if ((ac > 1 && !ft_strcmp(av[1], "leaks"))
        || (ac > 2 && !ft_strcmp(av[2], "leaks"))
        || (ac > 3 && !ft_strcmp(av[3], "leaks")))
        while (1)
            ;
    return (0);
}
