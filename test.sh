# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    test.sh                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: aulima-f <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/12/05 13:26:58 by aulima-f          #+#    #+#              #
#    Updated: 2018/12/06 11:08:06 by aulima-f         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

GRN="\033[0;32m"
WHT="\033[1;37m"
BLU="\033[1;34m"
RED="\033[0;31m"
YEL="\033[1;33m"

folder=../
libft=../libft/

if [[ $2 ]]
then
    echo "$YEL>>>>>>>>$RED CHANGE BUFF_SIZE :$WHT $2."
	sed -i -e "s/# define BUFF_SIZE.*/# define BUFF_SIZE $2/" ../get_next_line.h
	rm ../get_next_line.h-e
	sh test.sh cmp
fi

if [ -f $1 ]
then
    echo "$YEL>>>>>>>>$RED OUPS : need params"
    echo "$YEL - first params :
	$GRN norme   $BLU=> run norminette
	$GRN cmp     $BLU=> compile all
	$GRN random  $BLU=> run gnl on /dev/random
	$GRN multi   $BLU=> rungnl on 3 files
	$GRN leaks   $BLU=> run leaks on 1 file
	$GRN leaks_m $BLU=> run leaks on 2 files
    $GRN binary $BLU=> run on one binary file
	$GRN clean 	 $GRN=> guess"
	echo "$YEL - second params :$RED change BUFF_SIZE (need number)"
fi

if [[ $1 = "cmp" ]]
then
    echo "$YEL>>>>>>>>$BLU COMPILE LIB :"
    make -C $libft fclean && make -C $libft
    echo "$YEL>>>>>>>>$BLU CREATE GNL OBJECT :"
    clang -Wall -Wextra -Werror -I ${libft}includes -o ${folder}get_next_line.o -c ${folder}get_next_line.c
    echo "$YEL>>>>>>>>$BLU CREATE MAIN OBJECT :"
    clang -Wall -Wextra -Werror -I ${libft}includes -I $folder -o main.o -c main.c
    echo "$YEL>>>>>>>>$BLU COMPILE ALL :"
    clang -o ${folder}test_gnl main.o ${folder}get_next_line.o -I ${libft}includes -L $libft -lft
fi

if [[ $1 = "random" ]]
then
    echo "$YEL>>>>>>>>$BLU TEST RANDOM :"
    ../test_gnl /dev/random
fi

if [[ $1 = "multi" ]]
then
    echo "$YEL>>>>>>>>$BLU TEST MULTI (3 files: 4lines, 10lines, void) :"
    echo > 4lines.txt "maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4"
    echo > 10lines.txt "maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4\nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10"
    ../test_gnl 4lines.txt 10lines.txt void.txt
fi

if [[ $1 = "leaks_m" ]]
then
    echo "$YEL>>>>>>>>$BLU TEST leaks (2 files: 4lines, 110lines) :"
    touch void.txt
    echo > 4lines.txt "maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4"
    echo > 110lines.txt "maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10
    "
    ../test_gnl 4lines.txt 110lines.txt leaks &
    	sleep 1
    	echo "\n$YEL[WAIT 5 seconds]"
	sleep 5
	leaks test_gnl > leaks.txt
	echo "$RED\nKILL PROG  (test_gnl) : $WHT" && pgrep test_gnl | xargs kill -9
	echo "$GRN\nDISPLAY leaks.txt : $WHT" && cat leaks.txt | grep "Process"
fi

if [[ $1 = "leaks" ]]
then
    echo "$YEL>>>>>>>>$BLU TEST leaks (1 file: 110lines) :"
    echo > 110lines.txt "maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10\n
    maison-line-1\nchateau-line-2\nchien-line-3\nchat-line-4
    \nloup-line-5\nbanane-line-6\npokemon-line-7\nmagic-line-8\nanimal-line-9\n42-line-10
    "
    ../test_gnl 110lines.txt leaks &
    	sleep 1
    	echo "\n$YEL[WAIT 5 seconds]"
	sleep 5
	leaks test_gnl > leaks.txt
	echo "$RED\nKILL PROG  (test_gnl) : $WHT" && pgrep test_gnl | xargs kill -9
	echo "$GRN\nDISPLAY leaks.txt : $WHT" && cat leaks.txt | grep "Process"
fi

if [[ $1 = "binary" ]]
then
    echo "$YEL>>>>>>>>$BLU TEST binary (get_next_line.o)$RED compar to wc -l + 1 or use 'sed -n {num line}p fich ../get_next_line.o' :"
    ../test_gnl ../get_next_line.o
fi

if [[ $1 = "clean" ]]
then
    echo "$YEL>>>>>>>>$BLU CLEAN"
    rm leaks.txt 10lines.txt 110lines.txt 4lines.txt main.o void.txt ${folder}get_next_line.o ${folder}test_gnl
    make -C $libft fclean
fi

if [[ $1 = "norme" ]]
then
    echo "$YEL>>>>>>>>$BLU NORME GNL .h and .c : $RED"
	norminette ${folder}*.c ${folder}*.h | grep -B 1 "Error" || echo "$GRN-----------------> [OK]"
    echo "$YEL>>>>>>>>$BLU NORME LIBFT .h : $RED"
	norminette ${libft}/includes/*.h | grep -B 1 "Error" || echo "$GRN-----------------> [OK]"
    echo "$YEL>>>>>>>>$BLU NORME LIBFT .c : $RED" 
	norminette ${libft}/srcs/*.c | grep -B 1 "Error" || echo "$GRN-----------------> [OK]"
fi
